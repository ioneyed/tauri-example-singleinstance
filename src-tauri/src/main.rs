// Author: ioneyed
// Purpose: Demonstrate running a single instance of Tauri
//          All arguments passed in via the `acme:` uri scheme
//          or by launching the application will be sent to
//          the first instance that was already opened in the
//          form of an event.

#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
use launcher::init_client::InitClient;
use launcher::init_server::{Init, InitServer};
use launcher::{OpenedRequest, OpenedResponse};
use netstat::{get_sockets_info, AddressFamilyFlags, ProtocolFlags, ProtocolSocketInfo};
use std::env;
use std::net::{IpAddr, SocketAddr};
use std::str::FromStr;
use sysinfo::{ProcessExt, System, SystemExt};
use tauri::{Manager, State};
use tonic::{transport::Server, Request, Response, Status};

struct Arguments(String);

pub mod launcher {
    tonic::include_proto!("launcher");
}

pub struct Opener {
    window: tauri::window::Window,
}

#[tonic::async_trait]
impl Init for Opener {
    async fn open(
        &self,
        request: Request<OpenedRequest>,
    ) -> Result<Response<OpenedResponse>, Status> {
        self.window
            .emit("opener", format!("{}", request.into_inner().name));
        let reply = launcher::OpenedResponse { accepted: true };
        Ok(Response::new(reply))
    }
}

#[tauri::command]
fn arg_parse(ags: State<Arguments>) -> String {
    ags.0.to_string()
}

fn existing(sys: System, name: String) -> (bool, SocketAddr) {
    let current_pid = sysinfo::get_current_pid().unwrap();
    let af_flags = AddressFamilyFlags::IPV4 | AddressFamilyFlags::IPV6;
    let proto_flags = ProtocolFlags::TCP | ProtocolFlags::UDP;
    let mut port: SocketAddr = SocketAddr::new(IpAddr::from_str("::1").unwrap(), 0);
    let mut found: bool = false;
    for proc in sys
        .process_by_name(&name)
        .iter()
        .filter(|proc| proc.pid() != current_pid)
    {
        for si in get_sockets_info(af_flags, proto_flags).unwrap() {
            if si
                .associated_pids
                .iter()
                .any(|spid| -> bool { *spid == proc.pid() as u32 })
            {
                found = true;
                match si.protocol_socket_info {
                    ProtocolSocketInfo::Tcp(_sitcp) => {
                        port = SocketAddr::new(_sitcp.local_addr, _sitcp.local_port);
                    }
                    ProtocolSocketInfo::Udp(_siudp) => {
                        port = SocketAddr::new(_siudp.local_addr, _siudp.local_port);
                    }
                }
            }
        }
    }
    (found, port)
}

async fn broadcast(arguments: String, addr: SocketAddr) -> Result<(), Box<dyn std::error::Error>> {
    let mut client = InitClient::connect(format!("http://{}", addr)).await?;
    let request = tonic::Request::new(OpenedRequest {
        name: arguments.into(),
    });
    let response = client.open(request).await?;
    // Do something with the response
    println!("{:?}",response);
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let sys = System::new_all();
    let (found, addr) = existing(sys, "applinks".to_string());
    let aw: String = env::args()
        .filter(|arg| !arg.contains(r#"applinks"#))
        .collect::<String>()
        .to_string();
    if !found {
        tauri::Builder::default()
            .setup(move |app| {
                let window = app.get_window("main").unwrap();
                tauri::async_runtime::spawn(async move {
                    let listener = Opener { window: window };
                    match Server::builder()
                        .add_service(InitServer::new(listener))
                        .serve(addr)
                        .await
                    {
                        Ok(()) => (),
                        Err(err) => {
                            print!("Error {:?}", err);
                        }
                    };
                });
                Ok(())
            })
            .manage(Arguments(aw.to_string()))
            .invoke_handler(tauri::generate_handler![arg_parse])
            .run(tauri::generate_context!("./tauri.conf.json"))
            .expect("error while running tauri application");
    } else {
        broadcast(aw, addr).await?;
    }
    Ok(())
}
